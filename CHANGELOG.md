# Changelog

## v2.2.1 - 2024-10-03

### Fixed
 - #10 Ajout affichage lien accueil dans le menu lors de la navigation mobile 

## v2.2.0 - 2024-07-05

### Added
 - Ajout d'un CHANGELOG.md
 - Ajout d'un README.md

 ### Changed
 - Compatibilité maximum 4

## v2.1.0 - 2023-07-19

### Changed
 - Compatibilité minimum 4.1
 - Etat du plugin stable

## Removed
 - Suppression fichier inutiles `/.gitattributes`

### Fixed
 - Pas de puce ni de padding pour la liste du portfolio
 - Suppression des references au mode du document pour réparer le portfolio

## v2.0.9 - 2023-06-26

## Changed
 - Adapation des CSS au markup de SPIP 4

## Fixed 
 -  Ne pas mettre de box si pas de forum activé

 ## v2.0.8 - 2023-02-27

### Changed
 - Ajout compatiblité 4.2

### Fixed
 - Correction CSS décoration du titre et lien du site dans le menu